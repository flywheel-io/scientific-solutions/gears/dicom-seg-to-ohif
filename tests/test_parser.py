"""Module to test parser.py"""

from unittest.mock import MagicMock

import pytest
from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_dicom_seg_to_ohif.parser import parse_config

dummy_config = {
    "task_id": "fake_task_id",
    "protocol_id": "fake_protocol_id",
    "debug": False,
}


@pytest.fixture
def mock_client(mocker):
    client = mocker.patch("fw_gear_dicom_seg_to_ohif.parser.FWClient")

    return client


def test_parse_config(tmp_path, mock_client):
    (tmp_path / "input_dicom_seg.dcm").touch()
    fake_dicom_seg = tmp_path / "input_dicom_seg.dcm"
    (tmp_path / "source_dicom.dicom.zip").touch()
    fake_source_dicom = tmp_path / "source_dicom.dicom.zip"

    def _get_input(name):
        nonlocal fake_dicom_seg
        nonlocal fake_source_dicom
        if name == "dicom_seg":
            return fake_dicom_seg
        elif name == "source_dicom":
            return fake_source_dicom
        else:
            return None

    gear_context = MagicMock(spec=GearToolkitContext)
    gear_context.config = dummy_config
    gear_context.get_input_path.side_effect = _get_input
    gear_context.work_dir = tmp_path / "work"
    gear_context.get_input.return_value = {
        "object": {"file_id": "fake_file_id"},
        "key": None,
    }

    debug, gear_args = parse_config(gear_context)

    assert not debug
    assert gear_args["task_id"] == "fake_task_id"
    assert gear_args["protocol_id"] == "fake_protocol_id"
    assert gear_args["dicom_seg_path"] == fake_dicom_seg
    assert gear_args["source_dicom_path"] == fake_source_dicom
    assert gear_args["source_dicom_file_id"] == "fake_file_id"
    assert gear_args["work_dir"] == tmp_path / "work"
    mock_client.assert_called_once()


def test_parse_config_missing_ids(tmp_path, mock_client, caplog):
    (tmp_path / "input_dicom_seg.dcm").touch()
    fake_dicom_seg = tmp_path / "input_dicom_seg.dcm"

    gear_context = MagicMock(spec=GearToolkitContext)
    gear_context.config = {
        "task_id": None,
        "protocol_id": None,
        "debug": False,
    }
    gear_context.get_input_path.return_value = fake_dicom_seg
    gear_context.work_dir = tmp_path / "work"

    with pytest.raises(SystemExit):
        parse_config(gear_context)
    assert "task_id or protocol_id must be inputted" in caplog.text
