from unittest.mock import MagicMock

import pytest

from fw_gear_dicom_seg_to_ohif.prepper import Prepper


@pytest.fixture
def mock_init(mocker):
    mocked = mocker.patch("fw_gear_dicom_seg_to_ohif.prepper.Prepper.__init__")
    mocked.return_value = None
    return mocked


@pytest.fixture
def mock_load_dicoms(mocker):
    load = mocker.patch("fw_gear_dicom_seg_to_ohif.prepper.Prepper.load_dicoms")

    return load


@pytest.fixture
def mock_load_dicom_seg(mocker):
    load = mocker.patch("fw_gear_dicom_seg_to_ohif.prepper.Prepper.load_dicom_seg")

    return load


@pytest.fixture
def mock_DICOMCollection_fromzip(mocker):
    coll = mocker.patch("fw_file.dicom.DICOMCollection.from_zip")

    return coll


@pytest.fixture
def mock_DICOM(mocker):
    dcm = mocker.patch("fw_gear_dicom_seg_to_ohif.prepper.DICOM")

    return dcm


@pytest.fixture
def mock_sniff(mocker):
    sniff = mocker.patch("fw_gear_dicom_seg_to_ohif.prepper.sniff_dcm")

    return sniff


@pytest.fixture
def mock_dcmread(mocker):
    read = mocker.patch("fw_gear_dicom_seg_to_ohif.prepper.dcmread")

    return read


@pytest.fixture
def mock_segment(mocker):
    segment = mocker.patch("fw_gear_dicom_seg_to_ohif.prepper.create_segments")

    return segment


def test_prepper_init(
    mock_load_dicoms,
    mock_load_dicom_seg,
    tmp_path,
):
    work_dir = tmp_path / "work"
    dicom_seg_path = tmp_path / "dicom_seg.dcm"
    source_dicom_path = tmp_path / "fake.dicom.zip"

    prep = Prepper(
        work_dir=work_dir,
        dicom_seg_path=dicom_seg_path,
        source_dicom_path=source_dicom_path,
    )

    assert prep.work_dir == work_dir
    assert prep.source_dicom_path == source_dicom_path
    assert prep.dicom_seg_path == dicom_seg_path
    assert not prep.dicom_seg
    assert not prep.segments
    assert not prep.dicoms
    mock_load_dicoms.assert_called_once()
    mock_load_dicom_seg.assert_called_once()


def test_load_dicoms_zipped(
    tmp_path,
    mock_init,
    mock_is_zipfile,
    mock_DICOMCollection_fromzip,
):
    work_dir = tmp_path / "work"
    dicom_seg_path = tmp_path / "dicom_seg.dcm"
    source_dicom_path = tmp_path / "fake.dicom.zip"

    mock_is_zipfile.return_value = True

    prep = Prepper(
        work_dir=work_dir,
        dicom_seg_path=dicom_seg_path,
        source_dicom_path=source_dicom_path,
    )

    prep.source_dicom_path = source_dicom_path

    prep.load_dicoms()

    mock_is_zipfile.assert_called()
    mock_DICOMCollection_fromzip.assert_called()


def test_load_dicoms_single(
    tmp_path, mock_init, mock_is_zipfile, mock_sniff, mock_DICOM
):
    work_dir = tmp_path / "work"
    work_dir.mkdir()
    dicom_seg_path = tmp_path / "dicom_seg.dcm"
    source_dicom_path = tmp_path / "fake.dcm"
    source_dicom_path.touch()

    mock_is_zipfile.return_value = False
    mock_sniff.return_value = False

    prep = Prepper(
        work_dir=work_dir,
        dicom_seg_path=dicom_seg_path,
        source_dicom_path=source_dicom_path,
    )

    prep.source_dicom_path = source_dicom_path

    prep.load_dicoms()

    mock_init.assert_called_once()
    mock_sniff.assert_called_once()
    mock_DICOM.assert_called_once()


def test_load_dicom_seg(
    tmp_path,
    mock_init,
    mock_is_zipfile,
    mock_dcmread,
    mock_segment,
):
    work_dir = tmp_path / "work"
    dicom_seg_path = tmp_path / "dicom_seg.dcm"
    source_dicom_path = tmp_path / "fake.dicom.zip"

    prep = Prepper(
        work_dir=work_dir,
        dicom_seg_path=dicom_seg_path,
        source_dicom_path=source_dicom_path,
    )

    prep.dicom_seg_path = dicom_seg_path
    prep.dicoms = MagicMock()

    mock_is_zipfile.return_value = False

    prep.load_dicom_seg()

    mock_init.assert_called_once()
    mock_dcmread.assert_called_once()
    mock_segment.assert_called_once()
