"""Module to test creator.py"""

from unittest.mock import MagicMock

import numpy as np
import pytest
from fw_http_client.client import ClientError

from fw_gear_dicom_seg_to_ohif.creator import Creator


@pytest.fixture
def mock_create_new_task(mocker):
    create = mocker.patch("fw_gear_dicom_seg_to_ohif.creator.Creator.create_new_task")

    return create


@pytest.fixture
def mock_check_task(mocker):
    check = mocker.patch("fw_gear_dicom_seg_to_ohif.creator.Creator.check_task")

    return check


@pytest.fixture
def mock_create_annotation(mocker):
    create = mocker.patch("fw_gear_dicom_seg_to_ohif.creator.Creator.create_annotation")

    return create


@pytest.fixture
def mock_init(mocker):
    mocked = mocker.patch("fw_gear_dicom_seg_to_ohif.creator.Creator.__init__")
    mocked.return_value = None

    return mocked


@pytest.fixture
def mock_check_annotations(mocker):
    check = mocker.patch("fw_gear_dicom_seg_to_ohif.creator.check_if_annotations_exist")

    return check


def test_creator_init(tmp_path, mock_create_new_task, mock_check_task):
    prepper = MagicMock()
    prepper.work_dir = tmp_path / "work"
    prepper.dicom_seg = MagicMock()
    prepper.segments = []
    prepper.dicoms = MagicMock()

    create = Creator(
        prepper=prepper,
        task_id="fake_task_id",
        protocol_id="fake_protocol_id",
        source_dicom_file_id="fake_file_id",
        annotation_description=None,
        if_annotations_exist="exit",
        assignee="susannahtrevino@flywheel.io",
        fw=MagicMock(),
    )

    assert create.work_dir == prepper.work_dir
    assert create.dicom_seg == prepper.dicom_seg
    assert create.segments == prepper.segments
    assert create.dicoms == prepper.dicoms

    assert create.task_id == "fake_task_id"
    assert create.protocol_id == "fake_protocol_id"
    assert create.source_dicom_file_id == "fake_file_id"
    assert create.assignee == "susannahtrevino@flywheel.io"
    assert create.task is None
    assert create.num_created_annotations == 0

    # Since task_id is not None, create_new_task should not be called
    mock_create_new_task.assert_not_called()
    mock_check_task.assert_called_once()


def test_check_task(mock_init, mock_check_annotations):
    prepper = MagicMock()
    fw = MagicMock()
    fw.get.return_value = "task goes here"

    create = Creator(
        prepper=prepper,
        task_id="fake_task_id",
        protocol_id="fake_protocol_id",
        source_dicom_file_id="fake_file_id",
        annotation_description=None,
        if_annotations_exist="exit",
        fw=fw,
    )

    create.task_id = "fake_task_id"
    create.if_annotations_exist = "exit"
    create.fw = fw

    create.check_task()
    assert create.task == "task goes here"
    mock_check_annotations.assert_called_once()


def test_check_task_except(mock_init, caplog):
    prepper = MagicMock()
    fw = MagicMock()
    fw.get.side_effect = ClientError()

    create = Creator(
        prepper=prepper,
        task_id="fake_task_id",
        protocol_id="fake_protocol_id",
        source_dicom_file_id="fake_file_id",
        annotation_description=None,
        if_annotations_exist="exit",
        fw=fw,
    )

    create.task_id = "fake_task_id"
    create.if_annotations_exist = "exit"
    create.fw = fw

    with pytest.raises(SystemExit):
        create.check_task()
    assert "did not return a valid task" in caplog.text


def test_create_new_task(mock_init):
    prepper = MagicMock()
    fw = MagicMock()

    create = Creator(
        prepper=prepper,
        task_id="fake_task_id",
        protocol_id="fake_protocol_id",
        source_dicom_file_id="fake_file_id",
        annotation_description=None,
        if_annotations_exist="exit",
        assignee="none",
        fw=fw,
    )

    create.protocol_id = "fake_protocol_id"
    create.source_dicom_file_id = "fake_file_id"
    create.fw = fw
    create.assignee = None

    create.create_new_task()
    assert create.task
    assert create.task_id


def test_create_new_task_except(mock_init, caplog):
    prepper = MagicMock()
    fw = MagicMock()
    fw.post.side_effect = Exception()

    create = Creator(
        prepper=prepper,
        task_id="fake_task_id",
        protocol_id="fake_protocol_id",
        source_dicom_file_id="fake_file_id",
        annotation_description=None,
        if_annotations_exist="exit",
        assignee=None,
        fw=fw,
    )

    create.protocol_id = "fake_protocol_id"
    create.source_dicom_file_id = "fake_file_id"
    create.fw = fw
    create.assignee = None

    with pytest.raises(SystemExit):
        create.create_new_task()
    assert "Failed to create new task" in caplog.text


def test_process_segments(mock_init, mock_create_annotation):
    prepper = MagicMock()
    fw = MagicMock()

    create = Creator(
        prepper=prepper,
        task_id="fake_task_id",
        protocol_id="fake_protocol_id",
        source_dicom_file_id="fake_file_id",
        annotation_description=None,
        if_annotations_exist="exit",
        fw=fw,
    )

    create.segments = [MagicMock()]
    create.num_created_annotations = 0
    create.process_segments()
    mock_create_annotation.assert_called_once()


def test_create_annotation(mock_init):
    prepper = MagicMock()
    fw = MagicMock()

    create = Creator(
        prepper=prepper,
        task_id="fake_task_id",
        protocol_id="fake_protocol_id",
        source_dicom_file_id="fake_file_id",
        annotation_description=None,
        if_annotations_exist="exit",
        fw=fw,
    )

    create.fw = fw
    create.dicoms = MagicMock()
    create.task_id = "fake_task_id"
    create.source_dicom_file_id = "fake_file_id"
    create.annotation_description = "test"
    create.num_created_annotations = 0

    segment = MagicMock()
    segment.polygons = [np.array(([[1.5, 3], [0, 1.5]]))]

    create.create_annotation(segment)
    fw.post.assert_called_once()
    assert create.num_created_annotations == 1
