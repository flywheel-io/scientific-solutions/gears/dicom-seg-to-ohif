"""Module to test seg_utils.py"""

from unittest.mock import MagicMock

import numpy as np

from fw_gear_dicom_seg_to_ohif import seg_utils


def test_Segment_array2poly():
    data = np.array(([[0, 0, 1, 1], [0, 0, 1, 1], [0, 0, 0, 0], [0, 0, 0, 0]]))
    seg = seg_utils.Segment(
        ref_sop="1.2.3.4",
        ref_seg=1,
        seg_label="test",
        dim_index=[1, 1],
        slice_data=data,
        ref_frame=1,
    )

    seg.array2poly()

    assert len(seg.polygons) == 1
    expected_poly = np.array(([[1.5, 3], [0, 1.5]]))
    np.testing.assert_array_equal(seg.polygons[0], expected_poly)


def test_map_ipp_to_sop():
    source_dicom = MagicMock()
    source_dicom.bulk_get.side_effect = [
        [[1, 0, 0], [0, 1, 0], [0, 0, 1]],
        ("1.1.1", "2.2.2", "3.3.3"),
    ]

    test_ipp = [0, 1, 0]
    expected_sop = "2.2.2"
    actual_sop = seg_utils.map_ipp_to_sop(source_dicom, test_ipp)

    assert expected_sop == actual_sop


def test_us_cielab_to_rgb_str():
    test_val = [40032, 49936, 36953]
    expected = "rgba(253, 83, 122, 0.2)"
    actual = seg_utils.us_cielab_to_rgb_str(test_val)

    assert expected == actual
