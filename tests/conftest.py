import pytest
from flywheel_gear_toolkit.testing.files import create_dcm
from fw_file.dicom import DICOMCollection


@pytest.fixture
def dcmcoll(tmp_path):
    """Return DICOMCollection with two files."""

    def _gen(**kwargs):
        for key, val in kwargs.items():
            create_dcm(**val, file=str(tmp_path / (key + ".dcm")))
        return DICOMCollection.from_dir(tmp_path)

    return _gen


@pytest.fixture
def mock_is_zipfile(mocker):
    zip = mocker.patch("zipfile.is_zipfile")

    return zip
