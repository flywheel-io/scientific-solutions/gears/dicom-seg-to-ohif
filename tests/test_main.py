"""Module to test main.py"""

import pytest

from fw_gear_dicom_seg_to_ohif.main import run


@pytest.fixture
def mock_prep(mocker):
    prep = mocker.patch("fw_gear_dicom_seg_to_ohif.main.Prepper")
    return prep


@pytest.fixture
def mock_create(mocker):
    create = mocker.patch("fw_gear_dicom_seg_to_ohif.main.Creator")
    return create


def test_run(tmp_path, mock_prep, mock_create):
    dummy_args = {
        "task_id": "fake_task_id",
        "protocol_id": None,
        "dicom_seg_path": tmp_path / "dicom_seg.dcm",
        "source_dicom_path": tmp_path / "dicom.zip",
        "source_dicom_file_id": "fake_file_id",
        "annotation_description": None,
        "if_annotations_exist": "exit",
        "assignee": "susannahtrevino@flywheel.io",
        "work_dir": tmp_path / "work",
        "fw": None,
    }

    exit_code = run(dummy_args)
    assert exit_code == 0
    mock_prep.assert_called_once()
    mock_create.assert_called_once()
    mock_create.return_value.process_segments.assert_called_once()
