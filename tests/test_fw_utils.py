"""Module to test utils.py"""

from contextlib import nullcontext as does_not_raise
from unittest.mock import MagicMock

import pytest
from fw_file.dicom import DICOMCollection

from fw_gear_dicom_seg_to_ohif import fw_utils


@pytest.fixture
def mock_get_uid(mocker):
    uid = mocker.patch(
        "fw_gear_dicom_seg_to_ohif.fw_utils.get_dicom_seg_referenced_sop"
    )
    uid.return_value = ["fake_sop"]

    return uid


@pytest.fixture
def mock_collect(mocker):
    collect = mocker.patch(
        "fw_gear_dicom_seg_to_ohif.fw_utils.collect_potential_dicom_files"
    )

    return collect


@pytest.fixture
def mock_check(mocker):
    check = mocker.patch("fw_gear_dicom_seg_to_ohif.fw_utils.check_all_dicom_files")

    return check


@pytest.fixture
def mock_DICOMCollection_fromzip(mocker):
    coll = mocker.patch("fw_file.dicom.DICOMCollection.from_zip")

    return coll


@pytest.fixture
def mock_DICOM(mocker):
    dcm = mocker.patch("fw_gear_dicom_seg_to_ohif.fw_utils.DICOM")

    return dcm


@pytest.fixture
def mock_check_dicom_file(mocker):
    check = mocker.patch("fw_gear_dicom_seg_to_ohif.fw_utils.check_dicom_file")

    return check


def test_dynamic_search(tmp_path, mock_get_uid, mock_collect, mock_check):
    fw_client = MagicMock()
    work_dir = tmp_path / "work_dir"
    dicom_seg_path = tmp_path / "dicom_seg.dcm"
    session_id = "fake_session_id"
    source_dicom_path = tmp_path / "dicom.zip"
    mock_check.return_value = source_dicom_path
    mock_collect.return_value = (
        [source_dicom_path],
        {source_dicom_path: "dicom_file_id"},
    )

    source_dicom, file_id = fw_utils.dynamic_search(
        fw_client, work_dir, dicom_seg_path, session_id
    )
    assert source_dicom == source_dicom_path
    assert file_id == "dicom_file_id"
    mock_get_uid.assert_called_once()
    mock_collect.assert_called_once()
    mock_check.assert_called_once()


@pytest.mark.parametrize(
    "file",
    [
        "tests/assets/test_get_dicom_seg_uid/test_01.dcm",
        "tests/assets/test_get_dicom_seg_uid/test_02.dcm",
    ],
)
def test_get_dicom_seg_referenced_sop(file):
    sops = fw_utils.get_dicom_seg_referenced_sop(file)
    expected_sops = ["2.16.840.1.114570.4.2.957651682729322362985817814987233753852708"]

    assert sops == expected_sops


def test_collect_potential_dicom_files(tmp_path):
    fw_client = MagicMock()
    work_dir = tmp_path / "work"
    session_id = "fake_id"
    dicom_seg_filename = "dicom_seg.dcm"
    returned_acquisitions = [
        {
            "files": [
                {"name": "dicom_seg.dcm", "type": "dicom"},
            ]
        }
    ]

    session = MagicMock()
    acquisitions = MagicMock()
    fw_client.get_session.side_effect = session
    session.return_value.acquisitions = acquisitions
    acquisitions.find.side_effect = [returned_acquisitions]

    paths = fw_utils.collect_potential_dicom_files(
        fw_client, work_dir, session_id, dicom_seg_filename
    )

    # Because "dicom_seg.dcm" is the same as the dicom_seg filename,
    # this file should not be downloaded.
    assert (work_dir / "potential_dicoms").exists()
    assert (tmp_path / "work/potential_dicoms/dicom_seg.dcm") not in paths


@pytest.mark.parametrize(
    "returned_files,expectation",
    [
        ([["correct_dcm.zip"], []], does_not_raise()),
        ([["correct_dcm.zip"], ["maybe_also_correct.zip"]], pytest.raises(SystemExit)),
        ([[], []], pytest.raises(SystemExit)),
    ],
)
def test_check_all_dicom_files(mock_check_dicom_file, returned_files, expectation):
    dicom_file_paths = ["correct_dcm.zip", "maybe_also_correct.zip"]
    mock_check_dicom_file.side_effect = returned_files

    with expectation:
        fw_utils.check_all_dicom_files(dicom_file_paths, sops_to_match=["some_uid"])


def test_check_dicom_file_zip(mock_is_zipfile, dcmcoll, mocker):
    mock_is_zipfile.return_value = True
    coll = dcmcoll()

    from_zip_mock = mocker.patch.object(DICOMCollection, "from_zip")
    from_zip_mock.return_value = coll
    sops = coll.bulk_get("SOPInstanceUID")

    dcm = fw_utils.check_dicom_file("fake/path.dcm", sops)

    assert dcm == "fake/path.dcm"
    mock_is_zipfile.assert_called_once()


def test_check_dicom_file_single(mock_is_zipfile, mock_DICOM):
    mock_is_zipfile.return_value = False

    dcm = fw_utils.check_dicom_file("fake/path.dcm", ["fake_uid"])

    assert not dcm
    mock_is_zipfile.assert_called_once()
    mock_DICOM.assert_called_once()


@pytest.mark.parametrize(
    "annotation,behavior,expected_log",
    [
        ({"count": 0}, "exit", "No existing annotations found"),
        (
            {"count": 1, "results": [{"_id": "fake_annotation"}]},
            "append",
            "annotations will be appended",
        ),
        (
            {"count": 1, "results": [{"_id": "fake_annotation"}]},
            "override",
            "deleting...",
        ),
    ],
)
def test_check_if_annotations_exist(annotation, behavior, expected_log, caplog):
    fw = MagicMock()
    fw.get.return_value = annotation

    fw_utils.check_if_annotations_exist(fw=fw, task_id="fake_id", behavior=behavior)

    assert expected_log in caplog.text
