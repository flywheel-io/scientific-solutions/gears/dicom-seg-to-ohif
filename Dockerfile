# python:3.11.6-slim-bullseye, 11/01/2023
# FROM python@sha256:ecaf89b8cedc4ff98b3a32df092d9197d30eee0dbae28a8fc9653c6e1323091f
FROM flywheel/python-gdcm:sse
ENV FLYWHEEL="/flywheel/v0"
WORKDIR ${FLYWHEEL}

RUN apk upgrade --no-cache && apk --no-cache add gcc g++ musl-dev linux-headers python3-dev

# # Dev install. git for pip editable install.
# RUN apt-get update &&  \
#     apt-get install --no-install-recommends -y git && \
#     apt-get clean && \
#     rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Installing main dependencies
COPY requirements.txt $FLYWHEEL/
RUN pip install --no-cache-dir -r $FLYWHEEL/requirements.txt

# Installing the current project (most likely to change, above layer can be cached)
COPY ./ $FLYWHEEL/
RUN pip install --no-cache-dir .

# Configure entrypoint
RUN chmod a+x $FLYWHEEL/run.py
ENTRYPOINT ["python","/flywheel/v0/run.py"]
