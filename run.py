#!/usr/bin/env python
"""The run script."""

import logging
import sys

from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_dicom_seg_to_ohif.main import run
from fw_gear_dicom_seg_to_ohif.parser import parse_config

log = logging.getLogger(__name__)


def main(context: GearToolkitContext) -> None:  # pragma: no cover
    """Parses gear config and run."""
    debug, gear_args = parse_config(context)

    e_code = run(gear_args)

    sys.exit(e_code)


# Only execute if file is run as main, not when imported by another module
if __name__ == "__main__":  # pragma: no cover
    # Get access to gear config, inputs, and sdk client if enabled.
    with GearToolkitContext() as gear_context:
        # Initialize logging, set logging level based on `debug` configuration
        # key in gear config.
        gear_context.init_logging()

        # Pass the gear context into main function defined above.
        main(gear_context)
