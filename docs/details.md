# Gear Details

This section provides detailed information about the gear, including its
inputs, outputs, and configuration options.

## License

*License:* *MIT*

## Classification

*Category:* *Converter*

*Gear Level:*

- [ ] Project
- [ ] Subject
- [ ] Session
- [X] Acquisition
- [ ] Analysis

## Inputs

### Files

#### dicom_seg

- __Name__: *dicom_seg*
- __Type__: *DICOM file*
- __Optional__: *False*
- __Description__: *DICOM SEG file with one or more segments*

#### source_dicom

- __Name__: *source_dicom*
- __Type__: *DICOM file*
- __Optional__: *True*
- __Description__: *Optional: Source DICOM referenced by the DICOM SEG*
- __Notes__: *A source DICOM file is required for the gear to run.
If not provided as input, the gear will search the session container
for a DICOM that corresponds with the inputted DICOM SEG. If a source
DICOM is not identified (or more than one is identified), the gear
will fail and log instructions to re-run the gear with source_dicom
explicitly inputted into the gear.*

### Config

#### debug

- __Name__: *debug*
- __Type__: *boolean*
- __Description__: *Log debug messages*
- __Default__: *False*

#### task_id

- __Name__: *task_id*
- __Type__: *string*
- __Description__: *Task ID of the task in which to store the created annotations.*
- __Optional__: *True*
- __Notes__: *Gear requires either task_id OR protocol_id. If neither are
provided, gear will exit and log an error requesting that the gear be
re-run with either a valid task_id or protocol_id provided.*

#### protocol_id

- __Name__: *protocol_id*
- __Type__: *string*
- __Description__: *Protocol ID of the protocol to use when creating a
new task, if no task_id.*
- __Optional__: *True*
- __Notes__: *Gear requires either task_id OR protocol_id. If neither are
provided, gear will exit and log an error requesting that the gear be
re-run with either a valid task_id or protocol_id provided. If both a
task_id and protocol_id are inputted, gear will strictly
utilize the task_id.*

#### annotation_description

- __Name__: *annotation_description*
- __Type__: *string*
- __Description__: *Additional information to be appended to each created annotation's
`description` field.*
- __Optional__: *True*

#### if_annotations_exist

- __Name__: *if_annotations_exist*
- __Type__: *string*
- __Description__: *Selected behavior if `task_id` is provided and other annotations
are already connected to the task. Options are `override`, `append`, `exit`.*
- __Default__: `exit`

#### assignee

- __Name__: *assignee*
- __Type__: *string*
- __Description__: *If creating a new task, the Flywheel user the task is to be assigned.
If left blank, the assignee will default to the user running the gear. User ID is the
login email address, i.e `susannahtrevino@flywheel.io`.*
- __Optional__: *True*

## Outputs

This gear outputs OHIF annotations attached to a task container, viewable
with the Flywheel Viewer. More information on tasks can be found
[here](https://docs.flywheel.io/User_Guides/user_introduction_to_read_tasks/).

Please note that this gear can only run on Flywheel instances where
read tasks are enabled. If you do not have read tasks enabled or are
unsure that your instance is set up for this, please contact <support@flywheel.io>.

### Output Files

No files are outputted by this gear.

### Metadata

No metadata is created or modified by this gear.

## Pre-requisites

### Prerequisite Gear Runs

No prerequisite gear runs are required by this gear.

### Prerequisite Files

No prerequisite files are required by this gear.

### Prerequisite Metadata

Prerequisite metadata are not required by this gear.
