# DICOM-SEG-to-OHIF

## Purpose

This Flywheel gear converts DICOM SEG ROIs to Flywheel OHIF annotations.

DICOM SEGs are segmentation files that include ROI segment information
and pixel data related to a source DICOM file. The dicom-seg-to-ohif gear
is provided so that these segments can be converted to OHIF annotations
compatible with the Flywheel viewer.

## Features

- Created OHIF annotations are stored on pre-existing or newly-created reader tasks.
- If pre-existing annotations exist, behavior can be set to keep
existing and append new annotations, override existing annotations
with new annotations, or exit without changes.
- Annotations can be created with description field populated with
additional information, like the algorithm and version that created
the original segmentation.

## Examples

![Example Output](assets/images/output.png)

## Notes

This gear can only run on Flywheel instances where
[read tasks](https://docs.flywheel.io/User_Guides/user_introduction_to_read_tasks/)
are enabled. If you do not have read tasks enabled or are unsure that your instance
is set up for this, please contact <support@flywheel.com>.
