# Frequently Asked Questions

??? info "What do I need to run this gear?"

    To run this Flywheel gear, you will need a DICOM SEG with one
    or more segments, the source DICOM to which the DICOM SEG
    correlates, and either a created task or a created protocol.

??? info "How do I specify where to put the output?"

    This gear can output annotations into a pre-existing task or it
    can create a new task with a pre-existing protocol. To use a
    pre-existing task, input the task_id 24-character BSON string into
    the config option `task_id`. To create a new task, input the
    protocol_id 24-character BSON string into the config option
    `protocol_id`. If both `task_id` and `protocol_id` are provided,
    the gear will prioritize `task_id` and ignore `protocol_id`. If
    neither `task_id` nor `protocol_id` are provided, the gear will exit.

??? info "What is a task?"

    This gear leverages Flywheel's Read Task containers to store annotations.
    More information on tasks can be found
    [here](https://docs.flywheel.io/User_Guides/user_introduction_to_read_tasks/).

??? info "I don't have a protocol. How do I create one?"

    To create a viewer protocol via the Flywheel UI, please reference the
    [Creating a Viewer Protocol](https://docs.flywheel.io/clinical/reader-studies/clin_creating_a_viewer_protocol/)
    documentation.

??? info "How can I differentiate between multiple annotation sets?"

    The config option `annotation_description` allows for a description
    to be attached to the created annotations. For example, to compare
    multiple DICOM SEG files that were created by different algorithms,
    `annotation_description` could be set to the algorithm name and version,
    so that they can be identified at a glance. These descriptions can be
    seen in the Flywheel Viewer when looking at the list of annotations.

??? info "How do I make sure no pre-existing annotations are deleted?"

    By default, the config option `if_annotations_exist` is set to `exit`.
    If the task designated to store the output annotations already contains
    annotations, the gear will log an error and exit without adding, deleting,
    or modifying any annotations. To append the new annotations to the task,
    set `if_annotations_exist` to `append`.

??? info "How do I make sure pre-existing annotations *are* deleted?"

    If all pre-existing annotations should be deleted from an existing task
    before the gear creates new annotations, set `if_annotations_exist` to
    `override`.

??? info "Where can I see the output?"

    There are two locations to access tasks within Flywheel. On
    the left navigation sidebar, Tasks is located under the DATA header.
    From here, you can see the tasks you have access to across the site.
    Additionally, from inside the project, the upper navigation bar
    provides a Tasks tab that, when clicked, displays the tasks related
    to the selected project.

??? info "What are some helpful OHIF configuration options?"

    To hide the measurement text box attached to each annotation, add
    `"hideROIOutput": True` to your viewer config.

    These configuration options are attached to the protocol. If creating
    the protocol via SDK script, change the `viewer_info` as below:

    ```python
    viewer_info = {
        "name": "DICOM SEG viewer config",  # Can be changed
        "config": {
            # This is where to put your chosen config options, i.e.
            "hideROIOutput": True,
        }
    }
    ```

    If the viewer config has already been created, the SDK can also be used
    to make changes, using the following to update the config:

    ```python
    fw.put('/api/viewerconfigs/<viewer_config_id>', json=viewer_info)
    ```

    Additional information on viewer config options can be found
    [here](https://docs.flywheel.io/clinical/viewer/clin_the_ohif_config_file/).
