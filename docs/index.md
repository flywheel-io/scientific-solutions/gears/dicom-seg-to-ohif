# DICOM-SEG-to-OHIF Gear Documentation

Welcome to the documentation for the DICOM-SEG-to-OHIF gear. This gear is designed
to convert DICOM SEG ROIs to Flywheel OHIF annotations.

In this documentation, you will find detailed information on how to use the
DICOM-SEG-to-OHIF gear, including usage examples, and advanced features.

Whether you are a developer looking to integrate the gear into your workflow or a user
who wants to learn how to import and manage file metadata effectively, this
documentation will provide you with all the necessary information.

Let's get started!
