# Usage and Workflow

## Usage

### Description

This gear takes as input a DICOM SEG with one or more segments. It
provides as output OHIF annotation representations of these segmentations.
These annotations are stored in a task container as specified by the
`task_id` or `protocol_id` config. If a task_id is specified, the
existing task will be utilized as the container for the annotations. If
protocol_id is specified, the gear will create a new task utilizing the
given protocol_id.

#### File Specifications

This gear can be run on a DICOM SEG with one or more segmentations,
where the source DICOM is either found in the same session container
as the inputted DICOM SEG or inputted explicitly.

## Workflow

1. Upload DICOM SEG to acquisition container
1. Upload source DICOM to acquisition container within the same
session as the DICOM SEG
1. Select DICOM SEG as input to gear
1. Gear attaches output OHIF annotations to task

## Use Cases

### Use Case 1: DICOM SEG with gear-identified source DICOM

__*Conditions:*__

- DICOM SEG and source DICOM are uploaded to the same session container
- Exactly one DICOM within the session container matches the DICOM SEG

After inputting the DICOM SEG and beginning the gear run, the gear will
search for the source DICOM within the session container. The gear
will then utilize the source DICOM as needed in conjunction with the
DICOM SEG to create the OHIF annotation output.

### Use Case 2: DICOM SEG with inputted source DICOM

__*Conditions:*__

- DICOM SEG and source DICOM are uploaded to Flywheel
- Source DICOM is not in the same session container, or multiple
DICOMs in the session container match the DICOM SEG

The DICOM SEG and source DICOM must both be provided as inputs. The
gear will utilize the inputted source DICOM instead of searching
for the file within the session container.

### Use Case 3: Attaching OHIF annotations to already-existing task

__*Conditions:*__

- Task with which to contain the annotations is already created

By inputting the `task_id` of the already-created task, this gear will
connect all outputted annotations to the designated task.

### Use Case 4: Comparing two sets of segmentations in OHIF

__*Conditions:*__

- Task with which to contain the annotations is already created
- Multiple DICOM-SEG files exist for the same source DICOM
- User wants to compare the two DICOM-SEG annotations

By running this gear twice with two different DICOM-SEG files and configuring
the gear run with the same `task_id` and setting `if_annotations_exist="append"`,
both gear runs will add annotations to the same task container. By configuring
`annotation_description` with contrasting text (i.e. information about how the
DICOM-SEG was created), these annotations can be better identified while
utilizing the Flywheel Viewer.

### Use Case 5: Attaching OHIF annotations to new task with existing protocol

__*Conditions:*__

- New task is needed to contain the annotations
- Protocol with which to create a new task exists

By inputting the `protocol_id` of the protocol to be used to create a
new task, the gear will initialize a new task with the given protocol,
allowing protocol configurations to be re-used for each new task.

### Use Case 6: Setting the new task assignee

__*Conditions:*__

- New task is needed to contain the annotations
- Protocol with which to create a new task exists
- Task should be assigned to a user other than the user running the gear

By inputting both a `protocol_id` as in Use Case 5 as well as an `assignee`,
the task will be assigned to the user provided in the `assignee` config option.
User should be specified by their user ID (i.e. `susannahtrevino@flywheel.io`)
and must be a valid user on the Flywheel instance storing the task.

### Use Case 7: Attaching OHIF annotations to new task, no existing protocol

__*Conditions:*__

- New task is needed to contain the annotations
- A protocol with which to create a new task does not yet exist

In Flywheel, all read tasks require a protocol. This protocol sets a variety
of configuration options, and without a protocol, any attached annotations
would not be viewable within the Flywheel Viewer.

To create a viewer protocol via the Flywheel UI, please reference the
[Creating a Viewer Protocol](https://docs.flywheel.io/clinical/reader-studies/clin_creating_a_viewer_protocol/)
documentation.

Additionally, a viewer protocol can be created via a python script. The below code
snippet can be used for this purpose, leveraging FWClient to make API calls:

```python
from fw_client import FWClient

fw = FWClient(api_key=api_key)  # Insert your API key here

form_info = {
    "viewer": "OHIF",
    "parent": {
        "type": container,  # site/project
        "id": parent_id  # id of container
    },
    "form": {}  # If desired, a form can be configured here;
    # left blank will initialize the task without an attached form.
}

form = fw.post('/api/forms', json=form_info)

viewer_info = {
    "name": "DICOM SEG viewer config",  # Can be changed
    "config": {}  # If desired, viewer config options can be added here
    # left blank will initialize the default viewer.
}

viewer = fw.post('/api/viewerconfigs', json=viewer_info)

protocol_info = {
    "label": "DICOM SEG to OHIF",  # Can be changed
    "description": "Protocol for DICOM SEG to OHIF",  # Can be changed
    "form_id": form._id,
    "viewer_config_id": viewer._id,
    "parent": {
        "type": container,  # Match the container provided for form_info
        "id": parent_id  # Match the parent_id provided for form_info
    }
}

protocol = fw.post('/api/read_task_protocols', json=protocol_info)

print(protocol._id)
```

## Logging

This gear logs information about the gear run, including configuration settings,
segments found within the DICOM SEG, and calls made to the Flywheel API.
If the gear run encounters a known error, the gear logs what occurred and, if
applicable, what to do to fix the error in a subsequent run.
